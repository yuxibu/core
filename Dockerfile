FROM moodlehq/moodle-php-apache:8.2-bookworm

# Atualiza os pacotes e instala o wget
RUN apt update && apt install -y wget && apt-get clean
#Coloca o diretório de trabalho no diretório raiz do servidor web
WORKDIR /var/www
# Baixa o pacote do Moodle e o descompacta diretamente, sem precisar salvar o pacote localmente.
# Nota: A URL direta para o download do arquivo pode mudar, verifique se esta é a mais atual para o Moodle 4.0.3
RUN wget -q -O- "https://download.moodle.org/download.php/direct/stable403/moodle-latest-403.tgz" | tar xz -C .
# Remove o diretório padrão do Apache
RUN rm -rf html
#Renomeia o diretório para o nome padrão do Apache
RUN mv moodle html
# Copia o arquivo de configuração para o diretório correto
COPY ./config.php /var/www/html
