Serviço Central do Site YUXIBU.ORG
==================================

Este documento descreve os passos para a configuração e execução do serviço central do projeto, incluindo detalhes sobre volumes relevantes.

[TOC]

Configuração
------------

Para preparar o ambiente necessário para o serviço, siga os passos abaixo:

1. **Crie um arquivo de configuração `.env`**:
   - Copie o arquivo `example.env` para `.env`.
   - Preencha o arquivo `.env` com as informações relativas à configuração do banco de dados, conforme o exemplo a seguir:

     ```env
     MYSQL_ROOT_PASSWORD=minhasenharoot
     MYSQL_DATABASE=nomedobanco
     MYSQL_USER=nomedousuario
     MYSQL_PASSWORD=senhadousuario
     ```

2. **Aplicação da Configuração**:
   - As informações do arquivo `.env` serão utilizadas para configurar o arquivo `config.php` do Moodle durante a criação do container.

Execução
--------

Para iniciar o serviço, proceda da seguinte forma:

1. **Construa a imagem e inicie o container**:
   - Execute o comando abaixo no terminal:

     ```bash
     $ docker compose up
     ```

2. **Acesso ao Serviço**:
   - Após a inicialização, acesse `localhost:9090` no seu navegador para iniciar o processo de instalação do Moodle.

Volumes
-------

A configuração do `docker-compose` inclui dois volumes nomeados, essenciais para o armazenamento de dados:

- **`moodledata`**: Armazena os dados dos usuários.
- **`mysql_data`**: Contém os arquivos do banco de dados.